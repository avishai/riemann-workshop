# Riemann events

Events are protobuff objects on the wire and clojure maps in Riemann.
The general structure is:

    #{ :time :host :service :state :metric :description :tags }

We can have any other attribute associated with the map, but the protobuff strucuture defines generic attributes as string->string key value items.

The protobuff structure is:

    message Event {
      optional int64 time = 1;
      optional string state = 2;
      optional string service = 3;
      optional string host = 4;
      optional string description = 5;
      repeated string tags = 7;
      optional float ttl = 8;
      repeated Attribute attributes = 9;
      optional sint64 metric_sint64 = 13;
      optional double metric_d = 14;
      optional float metric_f = 15;
    }

The full protobuff file can be found [here](https://github.com/aphyr/riemann-java-client/blob/master/src/main/proto/riemann/proto.proto)