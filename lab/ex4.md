# Cluster monitoring

### Clock skew

One would be tempted to use

    (clock-skew index)

But we have to beware: `clock-skew` only changes `metric`, not service name. Also we want to handle only events that orignated from the nodes (not external probes)

    (where (not (tagged "external-probe"))
        (clock-skew
            (with {:service "clock-skew" :ttl 120}
                index)))

Let's find the 3 most deviant clocks in our cluster by replacing `index` with:

    (top 3 #(Math/abs (:metric %)) 
        (adjust [:tags conj "deviant-clock"]
            index))

### Average X in the cluster

    (where (tagged "linux-basic-metrics")
        (by [:service]
            (coalesce
                (smap folds/mean
                    (with :host "aggregated" 
                        (adjust [:service str " (avg)"]
                            index))))))

### How many hosts in the cluster?

Let's define a new function

    (require '[clojure.string :as string]) ; for string/join
    (defn count-aggr-host [& children]
        (fn [events]
            (let [events (remove nil? events)
                  e (first events)
                  e (assoc e :metric (count events) :description (string/join " " (map attr events)) :host nil :tags [] :ttl 60)]
                  (call-rescue e children))))

The use it to count all hosts 

    (where (tagged "linux-basic-metrics")
        (with :service "hosts"
            (coalesce
                (smap (count-aggr-host index)))))

Same with native clojure state:

    (let [hosts (atom #{})]
        (where (tagged "linux-basic-metrics")
            (throttle 1 10
                (fn [event]
                    (swap! hosts conj (:host event))
                    (index {:service "hosts" :ttl 60 :metric (count @hosts) :time (:time event)}))
                ))
        (where* (fn pred [event] (and ((tagged "action") event) (= (:action event) "remove-host")))
            (fn [e] (swap! hosts disj (:host e)))
        ))