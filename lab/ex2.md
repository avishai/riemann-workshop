# TTL and expired events

We can use TTL to expire events and detect stale status.

### Changed state

    (by [:host :service]
        (changed :state
            warn))
    (changed-state {:init "ok"} warn)

There will be no status in the dashboard uless we index the expired event

### Down host with recovery notification and dashboard status

    (match expired? false
        (with {:service "alive" :ttl 30} index))
    (expired
        (where (service "alive")
            (with {:status "down" :ttl -1} index))) ; we index the down state

So how do we finally remove the host from the index?

    (where (tagged "action-delete")
        (delete-from-index))

For autoscaled hosts we can give a positive `:ttl` value.