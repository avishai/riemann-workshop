# Graphing and alerting

## Async queue

    (let [graph-with-graphite (async-queue! 
            :graphite {:queue-size 1000}
            (by [:host :service]
                (throttle 10 1(graphite {:host "graphite"})})))
         ]
         (where (tagged "linux-basic-metrics") graph-with-graphite)
    )