# Rollups, throttling

We want to have a max of 2 messages per 10 minutes

    (let [email (mailer)]
        (changed-state (throttle 2 600 email))
    )

Rollup is like throttle but will not lose events, instead it accomulates them

    (rollup 0 600 email)

At the end of the 10 minutes time window a single email will be sent with all then events

Alert only if state is the same for 3 consecutive events

    (runs 3 :state email)
