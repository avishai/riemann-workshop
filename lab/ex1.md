# Exercise 1: Simple alerts

Alert on some metric value. To generate events we will use riemann-tools:

	riemann-health

### Alert on threshold using a simple check

    (where (and (service "memory") (> metric 0.8))
        (with {:description "memory over 80% used"}
            ;(email "joe_sysadmin@somecorp.com")
            prn
        )
    )
    (where (service "disk /")
        (under 0.2
            (info "This is weird, has someone erased too much data?"))
    )

### Alert on threshold using a range spec

    (where (service "cpu")
        (within [0.8 1]
            (with {:status "omg"}
                prn))
        (without [0 0.8]
            (with {:status "sh!t"}
                prn))
    )

### Split on predicates

    (split
        (< 0.9 metric) (with :state "critical" index)
        (< 0.7 metric) (with :state "warning" index)
        (with :state "ok" index) ; else
    )

### Exact match

    (where (service "exceptions")
        (match :description "horrible terrible exception which should never happen"
            (email "joe_developer@somecorp.com")))

### Dual

    (let [ncores (.. Runtime getRuntime availableProcessors)]
        (where (service "load")
            (dual #(> (:metric %) ncores)
                (with {:description "overload"} warn)
                (with {:description "still good"} info)
            )
        ))