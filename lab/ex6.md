# Time windows

    (moving-time-window 600 
        (combine folds/std-dev (ajdust [:service str " (stddev 10min)"] index)))

    (rate 60 (adjust [:service str " (1m rate)"] index))

Only alert if `state` is `stable` for 120 seconds. Note the use of `let`, we can't use `stable` with expired events because they only appear once.

    (let [changed-state-mail (changed-state (email "admin@somecorp.com"))])
        (dual expired?
            changed-state-mail ; we want expired events immediately
            (stable 120 :state changed-state-mail)))
