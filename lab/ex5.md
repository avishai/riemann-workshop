# Processing across services/hosts

## coalesce

Coalesce remembers the latest event for every `[:host :service]` value. When a new event arrives it calls `children` with all remembered events.

Total request count with coalesce

    (where (service "http requests")
        (coalesce 
            (combine 
                (fn [events] (assoc (first events) :metric (reduce + (map :metric events))))
                (with {:host nil tags ["aggregated"] :service "total http requests"} index)
        )))

## Project

    (where :host "lb"
        (project [(service "requests") (service "host-count")] 
            (combine (fn combine-fn [events]
                    (assoc (first events) :metric (reduce / (map :metric events))))
                (with :service "requests per host" index))))

Project is very much like `coalesce` except that it matches on pridacate results - its indexing key is `[(pred1 event) (pred2 event) ...]`. At least one predicate must match.

Use `coalesce` when you want to compute aggregate of events with unknown cardinality. Use `project` for fixed cardinality (e.g. specific services or hosts). 
