(require '(rieman-client :refer :all))

(defn hostname (.. java.net.InetAddress getLocalHost getHostName))

(defn riemann-client (tcp-client {:host "localhost"}))

(defn report-timer [_name timer-value]
  (aync-send-event riemann-client {:service (str "Timer" _name) :metric timer-value :ttl 60 :host hostname}))

(defmacro timed [_name forms] 
  `(let [start# (System/curTimeMillis) 
         res# ~forms
         ]
     (report-timer ~_name (- System/curTimeMillis start#))
     res#
   )
)
