# Streams

Riemann processes events using _streams_. Streams are functions and we can generate our own:

    (fn [event] (prn "got event" event))

is a stream.

Most stream expressions in Riemann are functions or macros that dynamically generate stream functions.

    (defn match [f value & children]
      (fn stream [event]
        (when (riemann.common/match value (f event))
          (call-rescue event children)
          true)))

Streams can be generated with an internal state using clojure data structures.

## Composing streams

Streams can be composed like any other function

    (let [changed-state-email (changed-state email)]
        (where :service "important" changed-state-email)
    )

Or using normal function definition

    (defn alert-if-overload []
        (where (> metric 0.9)
            (with :state "critical"
                (rollup 2 3600
                    ((mailer) "manager@somecorp.com")))))
    (where (and (service "disk /") (host "main-db")) (alert-if-overload))

To call children from within a composed stream we use `call-rescue`

    (defn maybe-wrap-stream [& children]
        (if (> 0.5 (rand))
            (call-rescue children)
            (ajdust [:tags conj "rand-match"] children)))

## Working with stateful streams

Consider this stream

    (split
        (> metric 0.9) (with :state "critical" email (rate 5 (adjust [:service str " rate"] index)))
        (> metric 0.7) (with :state "warning" (rate 5 (adjust [:service str " rate"] index)))
        (rate 5 index)
    )

This will show the wrong results because each _rate_ construct is a different stream with a separate internal state.

    (let [idx_rate (rate 5 (adjust [:service str " rate"] index)]
        (split
            (> metric 0.9) (with :state "critical") email idx_rate)
            (> metric 0.7) (with :state "warning" idx_rate)
            idx_rate))

Or:

    (smap (fn [e] 
        (let [_metric (:metric e)]
            (assoc e :state (cond
                (> _metric 0.9) "critical"
                (> _metric 0.7) "warning"
                :else (:state e)))))
        (where (state "critical") email)
        (rate 5 (adjust [:state str " rate"] index)))
