# Riemann workshop

## What is Riemann

Events:
- messages
- Contain certain fields
- :host, :service, :ttl, :time, :description

Riemann is an event processor
- Events go in
- Events get tagged, classified
- Events are correlated with memory
- Events go out
- Events expire (ttl)
- Side-effects

Riemann has memory
- The index - memory you can look at
- Clojure atoms, refs - internal private memory

Streams
- A pipeline of events
- Can be composed
- May have sub-streams (children)

Riemann is ephemeral
- No persistence
- State expires

## What Riemann is not

- Probe
- Metric collector
- Graphite (metrics store)
- Active monitoring
- Alerts management and workflow - but it can serve as a resonable alerting system
- Real Time BI - but it can be used for that as well

## Basic concepts

- Streams
- The index
- Ttl and expired events
- Keeping state outside the index (atoms)

## Tools

- Riemannq
- the dashboard
- riemann tools
- bernhard

## Comunicating with Riemann

- TPC/UDP (native protobuff)
- Websocket/SSE (query interface)
- Graphite/Statsd

## Using streams

- filtering (where, within, over/under, match, dual)
- splitting (by)
- combining (project, coalesce, top)
- changing event data (with, default, scale, smap)
- changed (state)
- rate, time windows, (rate, fixed-*-window, moving-*-window)
- statistics, math (precentiles, ddt, ewma ,apdex)

## Monitoring patterns

- TTL and expiry





