; -*- mode: clojure; -*-
; vim: filetype=clojure

(logging/init :file "riemann.log")

; Listen on the local interface over TCP (5555), UDP (5555), and websockets
; (5556)
(let [host "127.0.0.1"]
  (tcp-server :host host)
  (udp-server :host host)
  (ws-server  :host host))

; Expire old events from the index every 5 seconds.
(periodically-expire 5)

; Keep events in the index for 0.5 minutes by default.
(let [index (default :ttl 30 (update-index (index)))]

  (streams
    (match expired? false ; only unexpired events
      (adjust (fn [event] {:state "up" :service "alive" :ttl 30 :host (:host event) :time (:time event)}) 
        index))
    
    (expired
        (where (service "alive")
            (with {:state "down" :ttl -1} index))) ; ttl -1 so it stays in the index indefinitely
))
