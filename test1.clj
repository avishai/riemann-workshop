(let [host "127.0.0.1"]
  (tcp-server :host host)
  (udp-server :host host)
  (ws-server  :host host))

(periodically-expire 5)

(let [index (default :ttl 30 (update-index (index)))]
  (streams
    index
    (stable 10 :state (partial prn "stable"))
    (expired (partial prn "expired"))
  )
)
