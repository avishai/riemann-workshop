# The Riemann Index

Riemann has an internal map which indexes events by `[:host :service]`.
This is what gives Riemann the power to compare in-transit events with previous state, save data for visualization, etc.

The index can be queried using a simple query DSL from various transports, e.g. protobuff or websocket.

## Query DSL

The query DSL is documented in the test suite, but here are the main highlights:

The query DSL *is not* clojure, but a mini-language with its own rules. You can group with parenthesis as you would with "normal languages".

- Choose by service `service = "cpu"`, `service =~ "http%"` - note the '=~' matcher and '%' wildcard
- Other fields you query on: host, description, ttl, time, metric, metric_f
- Basic predicates: >,<, =, >=,<=,!=
- Query on tags: `tagged "linux-basic-metrics"`

Examples:

- `(host != "aggregate") and (tagged "linux-health" or service =~ "HTTP%")`
- `host != nil`

## Expired events

The index is periodically sweeped for events for which `now > ttl + timestamp`. These events are not discarded immediately, instead they are returned to the `streams` pipeline `(with :state "expired")`.
When an event is expired Riemann will unset all attributes other then `[:host :service]` - this is a common cause for `java.lang.NullPointerException`.

The behaviour of how an event is expired can be controlled:

    (periodically-expire 10 {:keep-keys [:host :service :metric]})